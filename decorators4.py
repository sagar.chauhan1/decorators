
from functools import lru_cache

@lru_cache(maxsize = 5)
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    return fact
print(factorial(5))
print(factorial(4))
print(factorial(6))
print(factorial(7))
print(factorial(5))
print(factorial(4))