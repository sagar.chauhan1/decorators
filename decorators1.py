import math
import time

def timer(function):
    def inner(arg):
        start = time.time()
        result = function(arg)
        end = time.time()
        print('Time Elapsed is {}\nResult is {}'.format((end - start), result))
    return inner


@timer
def sqrt(x):
    return math.sqrt(x)


@timer
def square(x):
    return x * x

print(sqrt(10000))
print(square(150))

