import time
def generic_decorators(function):
    def inner(*args, **kwargs):
        start = time.time()
        result = function(*args, **kwargs)
        end = time.time()
        print(function.__name__)
        print('Time Elapsed is {}\nResult is {}'.format((end - start), result))
    return inner


@generic_decorators
def add(x,y):
    return x+y


@generic_decorators
def add3(x, y, z):
    return x + y + z


@generic_decorators
def add_any(*args):
    return sum(args)


@generic_decorators
def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total
print(add(4,5))
print(add3(89,987,98765))
print(add_any(234,34,5345,345,34,534,53,45))
print(abs_add_any(23,23,24,342,343,433,43,455,4,abs=True))
