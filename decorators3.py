
import time
def memoize(function):
    cache_mem = {}
    def inner(arg):
        start = time.time()
        if cache_mem.get(arg):
            end = time.time()
            print('Time taken using the cache memory is : {}'.format(end - start))
            return cache_mem.get(arg)
        result = function(arg)
        cache_mem[arg] = result
        end = time.time()
        print('Time taken to execute the function is : {}'.format(end - start))
        return result
    return inner



@memoize
def fib(n):
    print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)

print(fib(6))
print(fib(9))
print(fib(6))

